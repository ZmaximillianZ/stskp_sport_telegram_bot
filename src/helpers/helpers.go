package helpers

import (
	"math/rand"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func GetMapKeyByValue(m map[int]string, value string) int {
	var result int
	for k, v := range m {
		if v == value {
			result = k
			break
		}
	}

	return result
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func stringWithCharset(length int, charset string) string {
	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func GetRandString(length int) string {
	return stringWithCharset(length, charset)
}

func MapGen() map[int]string {
	var mapSize = 10000
	result := make(map[int]string, mapSize)
	for i := 0; i < mapSize; i++ {
		result[i] = GetRandString(rand.Intn(100))
	}
	return result
}
