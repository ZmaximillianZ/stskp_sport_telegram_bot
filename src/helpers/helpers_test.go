package helpers

import (
	"testing"
)

var (
	testValue = "test"
	testKey   = 1
	testMap   = map[int]string{
		testKey: testValue,
	}
)

func TestGetMapKeyByValue(t *testing.T) {
	result := GetMapKeyByValue(testMap, testValue)
	if result != testKey {
		t.Errorf("test for GetMapKeyByValue Failed - error")
	}
}

func BenchmarkGetMapKeyByValue(b *testing.B) {
	m := MapGen()
	for i := 0; i < b.N; i++ {
		GetMapKeyByValue(m, "")
	}
}
