package config

import (
	"database/sql"
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/ZmaximillianZ/stskp_sport_telegram_bot/src/helpers"
)

type TelegramConfig struct {
	Token string
}

type DbConfig struct {
	Path string
}

type Config struct {
	Telegram TelegramConfig
	DbConfig DbConfig
}

func TConn(telegramToken string) (tgbotapi.UpdatesChannel, tgbotapi.BotAPI) {
	bot, err := tgbotapi.NewBotAPI(telegramToken)
	if err != nil {
		log.Panic(err)
	}
	// bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)
	helpers.CheckErr(err)

	return updates, *bot
}

func DbConn(dbPath string) sql.DB {
	dbConnection, err := sql.Open("sqlite3", dbPath)
	helpers.CheckErr(err)

	return *dbConnection
}

// New returns a new Config struct
func New() *Config {
	return &Config{
		Telegram: TelegramConfig{Token: getEnv("TELEGRAM_TOKEN", "")},
		DbConfig: DbConfig{Path: getEnv("DB_PATH", "")},
	}
}

// Simple helper function to read an environment or return a default value
func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}
