package handle

import (
	"encoding/json"
	"strconv"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"helpers"
	"repository"
	"structures"
	"widgets"
)

const AddWorkoutAction = "Add"
const CloseAction = "/close"
const OpenAction = "/open"
const OpenHelp = "/help"

type Handler struct {
	repository *repository.Repository
	botWidget  *widgets.BotWidget
}

func (handler *Handler) SetRepository(repository *repository.Repository) {
	handler.repository = repository
}

func (handler *Handler) SetWidget(widget *widgets.BotWidget) {
	handler.botWidget = widget
}

// Handle handle income requests
func (handler *Handler) Handle(updates tgbotapi.UpdatesChannel, bot tgbotapi.BotAPI) {
	for update := range updates {
		if update.Message != nil && !handler.checkStskpTeam(update.Message.From.UserName) {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
			msg.Text = "The stskp team only can use this bot. For more information please contact to @ZmaximillianZ"
			_, err := bot.Send(msg)
			helpers.CheckErr(err)
			continue
		}
		if update.CallbackQuery != nil {
			handler.HandleCallback(update, bot)
			continue
		}
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		switch update.Message.Text {
		case OpenAction:
			msg.ReplyMarkup = handler.botWidget.GetKeyboard()
		case CloseAction:
			msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
		case "/team":
			m := "stskp sport team: "
			for _, user := range handler.repository.GetUserInfo() {
				m += user.NickName + " "
			}
			msg.Text = m
		case OpenHelp:
			listOfTypeWorkout := handler.repository.GetHelp()
			listOfCommands := `
/open - open a key board
/close - close a key board
/team - list of stskp team`
			msg.Text = "This a list of training types: \n\n" + listOfTypeWorkout + listOfCommands
		case AddWorkoutAction:
			msg.ReplyMarkup = handler.botWidget.GetWorkoutTypesInlineKeyBoard(handler.repository.GetParentTypeWorkouts())
			user := handler.repository.GetUserByUserName(update.Message.From.UserName)
			handler.repository.AddWorkout("", user.UID)
		case "Larry", "Den", "Gektar", "Pablo", "Baton":
			msg.Text = handler.botWidget.ShowUserHistory(update.Message.Text)
		}

		_, err := bot.Send(msg)
		helpers.CheckErr(err)
	}
}

// handleCallback handle callback income requests
func (handler *Handler) HandleCallback(update tgbotapi.Update, bot tgbotapi.BotAPI) {
	if update.CallbackQuery.Message.Text == AddWorkoutAction {
		_, err := bot.AnswerCallbackQuery(tgbotapi.NewCallback(update.CallbackQuery.ID, "Uou choosed "+update.CallbackQuery.Data+" type workout"))
		helpers.CheckErr(err)
		msgInline := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Data)
		typeWorkout := handler.repository.GetTypeWorkoutByName(update.CallbackQuery.Data)
		if handler.repository.TypeWorkoutHasChild(typeWorkout.Name) {
			msgInline.ReplyMarkup = handler.botWidget.GetWorkoutTypesInlineKeyBoard(
				handler.repository.GetChildTypeWorkoutsByParentId(handler.repository.GetTypeWorkoutByName(update.CallbackQuery.Data).Id),
			)
		} else {
			msgInline.ReplyMarkup = handler.botWidget.GetCalculatorKeyboard(
				typeWorkout.Name,
				handler.repository.GetTypeWorkoutTemplatesByTypeWorkout(typeWorkout),
				[]widgets.CalculateOperation{widgets.CalculateOperation{TypeWorkout: update.CallbackQuery.Data}},
			)
		}
		_, errSend := bot.Send(msgInline)
		helpers.CheckErr(errSend)
		return
	}
	handler.HandleManager(handler.repository.GetTypeWorkoutByName(update.CallbackQuery.Message.Text), update, bot)
}

// HandleChildTypeWorkout handle child TypeWorkout
func (handler *Handler) HandleChildTypeWorkout(update tgbotapi.Update, bot tgbotapi.BotAPI) {
	typeWorkout := handler.repository.GetTypeWorkoutByName(update.CallbackQuery.Message.Text)
	calculateOperation := handler.GetCalculateOperationFromCallbackData(update.CallbackQuery.Data)
	if calculateOperation.Operation == "save" {
		handler.save(bot, update)
		return
	}
	calculateOperation.Calculate()
	var calculateOperations []widgets.CalculateOperation
	calculateOperations = append(calculateOperations, calculateOperation)
	calculatorKeyboard := tgbotapi.NewEditMessageReplyMarkup(
		update.CallbackQuery.Message.Chat.ID,
		update.CallbackQuery.Message.MessageID,
		handler.botWidget.GetCalculatorKeyboard(typeWorkout.Name, handler.repository.GetTypeWorkoutTemplatesByTypeWorkout(typeWorkout), calculateOperations),
	)
	_, err := bot.Send(calculatorKeyboard)
	helpers.CheckErr(err)
}

// HandleParentTypeWorkoutWithChild handle parent TypeWorkout with child
func (handler *Handler) HandleParentTypeWorkoutWithChild(update tgbotapi.Update, bot tgbotapi.BotAPI) {
	msgInline := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Data)
	typeWorkout := handler.repository.GetTypeWorkoutByName(update.CallbackQuery.Data)
	msgInline.ReplyMarkup = handler.botWidget.GetCalculatorKeyboard(
		typeWorkout.Name,
		handler.repository.GetTypeWorkoutTemplatesByTypeWorkout(typeWorkout),
		[]widgets.CalculateOperation{widgets.CalculateOperation{TypeWorkout: update.CallbackQuery.Data}},
	)
	_, err := bot.Send(msgInline)
	helpers.CheckErr(err)
}

// HandleCycleTypeWorkouts handle cylce type workouts
func (handler *Handler) HandleCycleTypeWorkouts(update tgbotapi.Update, bot tgbotapi.BotAPI) {
	calculateOperation := handler.GetCalculateOperationFromCallbackData(update.CallbackQuery.Data)
	if calculateOperation.Operation == "save" {
		handler.save(bot, update)
		return
	}
	calculateOperation.Calculate()
	typeWorkout := handler.repository.GetTypeWorkoutByName(update.CallbackQuery.Message.Text)
	var calculateOperations []widgets.CalculateOperation
	calculateOperations = append(calculateOperations, calculateOperation)
	calculatorKeyboard := tgbotapi.NewEditMessageReplyMarkup(
		update.CallbackQuery.Message.Chat.ID,
		update.CallbackQuery.Message.MessageID,
		handler.botWidget.GetCalculatorKeyboard(typeWorkout.Name, handler.repository.GetTypeWorkoutTemplatesByTypeWorkout(typeWorkout), calculateOperations),
	)
	_, err := bot.Send(calculatorKeyboard)
	helpers.CheckErr(err)
}

// GetCalculateOperationFromCallbackData return CalculateOperation structure by incoming data from inline button
func (handler *Handler) GetCalculateOperationFromCallbackData(data string) widgets.CalculateOperation {
	rawIn := json.RawMessage(data)
	bytes, errJson := rawIn.MarshalJSON()
	helpers.CheckErr(errJson)
	var calculateOperation widgets.CalculateOperation
	errJson = json.Unmarshal(bytes, &calculateOperation)

	return calculateOperation
}

// handleManager resolve which handle need to use
func (handler *Handler) HandleManager(typeWorkout structures.TypeWorkout, update tgbotapi.Update, bot tgbotapi.BotAPI) {
	switch typeWorkout.Type {
	case structures.TypeWorkoutCycle:
		handler.HandleCycleTypeWorkouts(update, bot)
	case structures.TypeWorkoutCompositeParent:
		handler.HandleParentTypeWorkoutWithChild(update, bot)
	case structures.TypeWorkoutComposite:
		handler.HandleChildTypeWorkout(update, bot)
	case structures.TypeWorkoutGymnastic:
		handler.HandleParentTypeWorkoutWithChild(update, bot)
	case structures.TypeWorkoutSingleCombat:
		return
	case structures.TypeWorkoutGame:
		return
	}
}

// check that stskp members only can use this bot
func (handler *Handler) checkStskpTeam(userName string) bool {
	user := handler.repository.GetUserByUserName(userName)
	if user.UID == 0 {
		return false
	}
	return true
}

// save save structures.TypeWorkoutValue structure
func (handler *Handler) save(bot tgbotapi.BotAPI, update tgbotapi.Update) {
	calculateOperation := handler.GetCalculateOperationFromCallbackData(update.CallbackQuery.Data)
	typeWorkout := handler.repository.GetTypeWorkoutByName(calculateOperation.TypeWorkout)
	typeWorkoutValue := new(structures.TypeWorkoutValue)
	if v, err := strconv.ParseFloat(calculateOperation.CurrentValue, 64); err == nil {
		typeWorkoutValue.Value = v
	}
	typeWorkoutTemplate := handler.repository.GetTypeWorkoutTemplateByTypeWorkoutAndFormat(
		typeWorkout,
		helpers.GetMapKeyByValue(structures.Formats, calculateOperation.Format),
	)
	typeWorkoutValue.TypeWorkoutTemplate = typeWorkoutTemplate
	typeWorkoutValue.Workout = handler.repository.GetLastInsertWorkoutId(handler.repository.GetUserByUserName(update.CallbackQuery.From.UserName).UID)
	if err := handler.repository.AddTypeWorkoutValue(*typeWorkoutValue); err != nil {
		_, err := bot.AnswerCallbackQuery(tgbotapi.NewCallback(update.CallbackQuery.ID, "woops! something went wrong, workout value didn't save"))
		helpers.CheckErr(err)
	}
	_, err := bot.AnswerCallbackQuery(tgbotapi.NewCallback(update.CallbackQuery.ID, calculateOperation.Format+" was saved"))
	helpers.CheckErr(err)
}
