package widgets

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"helpers"
	"repository"
	"structures"
)

const OperationPlus = "+"
const OperationPlus5 = "+5"
const OperationMinus = "-"

type CalculateOperation struct {
	Format       string `json:"f"`
	Operation    string `json:"o"`
	CurrentValue string `json:"v"`
	TypeWorkout  string `json:"t"`
}

func (c *CalculateOperation) Calculate() {
	currentValue, err := strconv.Atoi(c.CurrentValue)
	helpers.CheckErr(err)
	switch c.Operation {
	case OperationPlus:
		currentValue++
	case OperationMinus:
		currentValue--
	case OperationPlus5:
		currentValue += 5
	}
	c.CurrentValue = strconv.Itoa(currentValue)
}

type BotWidget struct {
	repository *repository.Repository
}

func (botWidget *BotWidget) SetRepository(repository *repository.Repository) {
	botWidget.repository = repository
}

func (botWidget *BotWidget) GetKeyboard() tgbotapi.ReplyKeyboardMarkup {
	var buttons []tgbotapi.KeyboardButton
	for _, user := range botWidget.repository.GetUserInfo() {
		buttons = append(buttons, tgbotapi.NewKeyboardButton(user.NickName))
	}
	return tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(tgbotapi.NewKeyboardButton("Add"), tgbotapi.NewKeyboardButton("/close")),
		tgbotapi.NewKeyboardButtonRow(buttons...),
	)
}

func (botWidget *BotWidget) GetCalculatorKeyboard(typeWorkout string, typeWorkoutTemplates []structures.TypeWorkoutTemplate, data []CalculateOperation) tgbotapi.InlineKeyboardMarkup {
	var inlineKeyButtons []tgbotapi.InlineKeyboardButton
	var inlineKeyboardRows [][]tgbotapi.InlineKeyboardButton
	inlineKeyboardRows = append(inlineKeyboardRows, tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData(typeWorkout, typeWorkout)))
	buttonValue := "0"
	for _, typeWorkoutTemplate := range typeWorkoutTemplates {
		for _, d := range data {
			if d.TypeWorkout != "" {
				typeWorkout = d.TypeWorkout
			}
			if structures.Formats[typeWorkoutTemplate.Format] == d.Format {
				buttonValue = d.CurrentValue
				break
			}
		}
		calculateOperationPlus := &CalculateOperation{
			Format:       structures.Formats[typeWorkoutTemplate.Format],
			Operation:    OperationPlus,
			CurrentValue: buttonValue,
			TypeWorkout:  typeWorkout,
		}
		calculateOperationPlus5 := &CalculateOperation{
			Format:       structures.Formats[typeWorkoutTemplate.Format],
			Operation:    OperationPlus5,
			CurrentValue: buttonValue,
			TypeWorkout:  typeWorkout,
		}
		calculateOperationMinus := &CalculateOperation{
			Format:       structures.Formats[typeWorkoutTemplate.Format],
			Operation:    OperationMinus,
			CurrentValue: buttonValue,
			TypeWorkout:  typeWorkout,
		}
		saveOperation := &CalculateOperation{
			Format:       structures.Formats[typeWorkoutTemplate.Format],
			Operation:    "save",
			CurrentValue: buttonValue,
			TypeWorkout:  typeWorkout,
		}
		calculateOperationPlusJson, errPlus := json.Marshal(calculateOperationPlus)
		//fmt.Println(string(calculateOperationPlusJson))
		helpers.CheckErr(errPlus)
		calculateOperationPlus5Json, errPlus := json.Marshal(calculateOperationPlus5)
		//fmt.Println(string(calculateOperationPlus5Json))
		helpers.CheckErr(errPlus)
		calculateOperationMinusJson, errMinus := json.Marshal(calculateOperationMinus)
		//fmt.Println(string(calculateOperationMinusJson))
		helpers.CheckErr(errMinus)
		saveOperationJson, errSave := json.Marshal(saveOperation)
		//fmt.Println(string(saveOperationJson))
		helpers.CheckErr(errSave)
		inlineKeyButtons = append(
			inlineKeyButtons,
			tgbotapi.NewInlineKeyboardButtonData(structures.Formats[typeWorkoutTemplate.Format], string(calculateOperationPlusJson)),
			tgbotapi.NewInlineKeyboardButtonData(OperationMinus, string(calculateOperationMinusJson)),
			tgbotapi.NewInlineKeyboardButtonData(OperationPlus5, string(calculateOperationPlus5Json)),
			tgbotapi.NewInlineKeyboardButtonData(buttonValue, string(calculateOperationPlusJson)),
			tgbotapi.NewInlineKeyboardButtonData("save", string(saveOperationJson)),
		)
		inlineKeyboardRows = append(inlineKeyboardRows, tgbotapi.NewInlineKeyboardRow(inlineKeyButtons...))
		inlineKeyButtons = nil
		buttonValue = "0"
	}
	return tgbotapi.NewInlineKeyboardMarkup(inlineKeyboardRows...)
}

func (botWidget *BotWidget) GetWorkoutTypesInlineKeyBoard(types []string) tgbotapi.InlineKeyboardMarkup {
	var inlineKeyButtons []tgbotapi.InlineKeyboardButton
	var inlineKeyboardRows [][]tgbotapi.InlineKeyboardButton
	var i int
	for _, typeWorkout := range types {
		i++
		inlineKeyButtons = append(inlineKeyButtons, tgbotapi.NewInlineKeyboardButtonData(strings.Replace(typeWorkout, "_", " ", -1), typeWorkout))
		if i == 2 {
			inlineKeyboardRows = append(inlineKeyboardRows, tgbotapi.NewInlineKeyboardRow(inlineKeyButtons...))
			i = 0
			inlineKeyButtons = nil
			continue
		}
	}
	if inlineKeyButtons != nil {
		inlineKeyboardRows = append(inlineKeyboardRows, tgbotapi.NewInlineKeyboardRow(inlineKeyButtons...))
	}
	return tgbotapi.NewInlineKeyboardMarkup(inlineKeyboardRows...)
}

func (botWidget *BotWidget) ShowUserHistory(NickName string) string {
	var (
		result                 = "\n"
		currentWorkoutDate     = time.Now()
		currentWorkoutDay      = currentWorkoutDate.Day()
		created                = fmt.Sprintf("%d-%02d-%02d", currentWorkoutDate.Year(), currentWorkoutDate.Month(), currentWorkoutDate.Day()) + " "
		currentTypeWorkoutName string
		typeWorkoutNameToPrint string
		currentFormatName      string
		formatNameToPrint      string
		historyRowWorkDay      int
	)
	for _, historyRow := range botWidget.repository.GetUserHistoryByName(NickName) {
		historyRowWorkDay = historyRow.Created.Day()
		if historyRowWorkDay < currentWorkoutDay {
			currentWorkoutDate = historyRow.Created
			currentWorkoutDay = currentWorkoutDate.Day()
			created = "\n\n" + fmt.Sprintf("%d-%02d-%02d", currentWorkoutDate.Year(), currentWorkoutDate.Month(), currentWorkoutDate.Day()) + " "
		}
		if currentTypeWorkoutName == historyRow.Name {
			typeWorkoutNameToPrint = ""
		} else {
			currentTypeWorkoutName = historyRow.Name
			typeWorkoutNameToPrint = "\n" + historyRow.Name + ": "
		}
		if currentFormatName == structures.Formats[historyRow.Format] && typeWorkoutNameToPrint == "" {
			formatNameToPrint = ""
		} else {
			currentFormatName = structures.Formats[historyRow.Format]
			formatNameToPrint = structures.Formats[historyRow.Format] + " - "
		}
		value := fmt.Sprintf("%d", int(historyRow.Value))
		result += created + typeWorkoutNameToPrint + " " + formatNameToPrint + value + ", "
		created = ""
	}
	return strings.Replace(result, ", \n", "\n", -1)
}
