package structures

import (
	"time"
)

const FormatMeters = 1
const FormatSeconds = 2
const FormatKilograms = 3
const FormatWatts = 4
const FormatRepetitions = 5
const FormatTemp = 6

const TypeWorkoutCycle = 1
const TypeWorkoutCompositeParent = 2
const TypeWorkoutComposite = 3
const TypeWorkoutGymnastic = 4
const TypeWorkoutSingleCombat = 5
const TypeWorkoutGame = 6

var Formats = map[int]string{
	FormatMeters:      "m",
	FormatSeconds:     "s",
	FormatKilograms:   "kg",
	FormatWatts:       "watt",
	FormatRepetitions: "rep",
	FormatTemp:        "temp",
}

type User struct {
	UID       int
	FirstName string
	LastName  string
	NickName  string
	DateBirth time.Time
	Created   time.Time
	UserName  string
}

type Workout struct {
	ID          int
	User        User
	Created     time.Time
	Description string
}

type TypeWorkout struct {
	Id          int
	ParentId    int
	Name        string
	Description string
	Type        int
}

type TypeWorkoutValue struct {
	Id                  int
	Workout             Workout
	TypeWorkoutTemplate TypeWorkoutTemplate
	Value               float64
}

type TypeWorkoutTemplate struct {
	Id          int
	TypeWorkout TypeWorkout
	Format      int
}

type UserHistory struct {
	Name    string
	Value   float64
	Created time.Time
	Format  int
}
