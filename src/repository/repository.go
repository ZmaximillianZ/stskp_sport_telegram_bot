package repository

import (
	"database/sql"
	"log"
	"time"

	"helpers"
	"structures"
)

type Repository struct {
	db *sql.DB
}

func (repository *Repository) SetDb(db *sql.DB) {
	repository.db = db
}

func (repository *Repository) GetUserInfo() []structures.User {
	rows, err := repository.db.Query("SELECT * FROM user")
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	user := new(structures.User)
	var users []structures.User
	for rows.Next() {
		err = rows.Scan(
			&user.UID,
			&user.FirstName,
			&user.LastName,
			&user.NickName,
			&user.DateBirth,
			&user.Created,
			&user.UserName,
		)
		helpers.CheckErr(err)
		users = append(users, *user)
	}

	return users
}

func (repository *Repository) GetUserByUserName(userName string) structures.User {
	rows, err := repository.db.Query("SELECT * FROM user where user_name like '%" + userName + "%'")
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	user := new(structures.User)
	for rows.Next() {
		err = rows.Scan(
			&user.UID,
			&user.FirstName,
			&user.LastName,
			&user.NickName,
			&user.DateBirth,
			&user.Created,
			&user.UserName,
		)
		helpers.CheckErr(err)
		break
	}
	return *user
}

func (repository *Repository) GetTypeWorkoutTemplateByTypeWorkoutAndFormat(typeWorkout structures.TypeWorkout, format int) structures.TypeWorkoutTemplate {
	typeWorkoutTemplate := new(structures.TypeWorkoutTemplate)
	err := repository.db.QueryRow(
		"SELECT id FROM type_workout_template where type_workout_id=? and format=?",
		typeWorkout.Id,
		format,
	).Scan(&typeWorkoutTemplate.Id)
	helpers.CheckErr(err)
	typeWorkoutTemplate.TypeWorkout = typeWorkout
	typeWorkoutTemplate.Format = format

	return *typeWorkoutTemplate
}

func (repository *Repository) GetTypeWorkoutTemplatesByTypeWorkout(typeWorkout structures.TypeWorkout) []structures.TypeWorkoutTemplate {
	rows, err := repository.db.Query("SELECT id, format FROM type_workout_template where type_workout_id=?", typeWorkout.Id)
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	var typeWorkouts []structures.TypeWorkoutTemplate
	for rows.Next() {
		_typeWorkout := new(structures.TypeWorkoutTemplate)
		err = rows.Scan(
			&_typeWorkout.Id,
			&_typeWorkout.Format,
		)
		helpers.CheckErr(err)
		_typeWorkout.TypeWorkout = typeWorkout
		typeWorkouts = append(typeWorkouts, *_typeWorkout)
	}
	return typeWorkouts
}

func (repository *Repository) GetTypeWorkoutById(id int) structures.TypeWorkout {
	typeWorkout := new(structures.TypeWorkout)
	err := repository.db.QueryRow("SELECT id, name FROM type_workout where id=?", id).Scan(&typeWorkout.Id, &typeWorkout.Name)
	helpers.CheckErr(err)

	return *typeWorkout
}

func (repository *Repository) GetTypeWorkoutByName(name string) structures.TypeWorkout {
	typeWorkout := new(structures.TypeWorkout)
	err := repository.db.QueryRow(
		"SELECT id, name, type FROM type_workout where name=?",
		name,
	).Scan(&typeWorkout.Id, &typeWorkout.Name, &typeWorkout.Type)
	helpers.CheckErr(err)

	return *typeWorkout
}

func (repository *Repository) GetParentTypeWorkouts() []string {
	rows, err := repository.db.Query("SELECT name FROM type_workout where parent_id IS NULL")
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	var typeName string
	var result []string
	for rows.Next() {
		err = rows.Scan(&typeName)
		helpers.CheckErr(err)
		result = append(result, typeName)
	}

	return result
}

func (repository *Repository) IsTypeWorkoutParent(name string) bool {
	var typeWorkoutParentId sql.NullInt32
	err := repository.db.QueryRow("SELECT parent_id FROM type_workout where name=?", name).Scan(&typeWorkoutParentId)
	helpers.CheckErr(err)
	return !typeWorkoutParentId.Valid
}

func (repository *Repository) TypeWorkoutHasChild(name string) bool {
	var countParentId int
	err := repository.db.QueryRow("SELECT count(parent_id) FROM type_workout where parent_id in (select id from type_workout where name=?)", name).Scan(&countParentId)
	helpers.CheckErr(err)
	return countParentId > 0
}

func (repository *Repository) GetChildTypeWorkoutsByParentId(parentTypeWorkId int) []string {
	rows, err := repository.db.Query("SELECT name FROM type_workout where parent_id=?", parentTypeWorkId)
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	var typeName string
	var result []string
	for rows.Next() {
		err = rows.Scan(&typeName)
		helpers.CheckErr(err)
		result = append(result, typeName)
	}

	return result
}

func (repository *Repository) GetWorkoutListByNickName(nickName string) []string {
	rows, err := repository.db.Query("SELECT description FROM workout left join user on workout.user_id=user.uid where user.nick_name='" + nickName + "'")
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	var description string
	var result []string
	for rows.Next() {
		err = rows.Scan(&description)
		helpers.CheckErr(err)
		result = append(result, description)
	}

	return result
}

func (repository *Repository) AddWorkout(description string, userId int) {
	tx, err := repository.db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into workout(user_id, created, description) values(?,?,?)")
	if err != nil {
		log.Fatal(err)
	}
	created := time.Now()
	defer func() {
		err := stmt.Close()
		helpers.CheckErr(err)
	}()
	_, err = stmt.Exec(userId, created, description)
	if err != nil {
		log.Fatal(err)
	}
	errCommit := tx.Commit()
	helpers.CheckErr(errCommit)
}

func (repository *Repository) GetLastInsertWorkoutId(userId int) structures.Workout {
	workout := new(structures.Workout)
	err := repository.db.QueryRow("SELECT id FROM workout where user_id=? order by id desc limit 1", userId).Scan(&workout.ID)
	helpers.CheckErr(err)

	return *workout
}

func (repository *Repository) AddTypeWorkoutValue(typeWorkoutValue structures.TypeWorkoutValue) error {
	tx, err := repository.db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("insert into type_workout_value(workout_id, type_workout_template_id, value) values(?,?,?)")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		err := stmt.Close()
		helpers.CheckErr(err)
	}()
	_, err = stmt.Exec(typeWorkoutValue.Workout.ID, typeWorkoutValue.TypeWorkoutTemplate.Id, typeWorkoutValue.Value)
	if err != nil {
		log.Fatal(err)
	}
	errCommit := tx.Commit()
	helpers.CheckErr(errCommit)
	return err
}

func (repository *Repository) GetUserHistoryByName(NickName string) []structures.UserHistory {
	rows, err := repository.db.Query("select tw.name, v.value, w.created, twt.format from type_workout_value v left join type_workout_template twt on v.type_workout_template_id = twt.id left join type_workout tw on twt.type_workout_id = tw.id left join workout w on v.workout_id = w.id left join user u on w.user_id = u.uid where v.value is not '0' and u.nick_name=? order by w.created  desc", NickName)
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	userHistory := new(structures.UserHistory)
	var userHistoryHub []structures.UserHistory
	for rows.Next() {
		err = rows.Scan(&userHistory.Name, &userHistory.Value, &userHistory.Created, &userHistory.Format)
		helpers.CheckErr(err)
		userHistoryHub = append(userHistoryHub, *userHistory)
	}

	return userHistoryHub
}

func (repository *Repository) GetHelp() string {
	rows, err := repository.db.Query("select distinct name, description from type_workout")
	helpers.CheckErr(err)
	defer func() {
		err := rows.Close()
		helpers.CheckErr(err)
	}()
	var (
		name        string
		description string
		help        string
	)
	for rows.Next() {
		err = rows.Scan(&name, &description)
		helpers.CheckErr(err)
		help += name + " - " + description + "\n"
	}

	return help
}
