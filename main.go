package main

import (
	"helpers"
	"log"

	"config"
	"github.com/joho/godotenv"
	_ "github.com/mattn/go-sqlite3"
	"handle"
	"repository"
	"widgets"
)

// init is invoked before main()
func init() {
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func main() {
	conf := config.New()
	updates, bot := config.TConn(conf.Telegram.Token)
	bot.Debug = true
	c := config.DbConn(conf.DbConfig.Path)
	handler := new(handle.Handler)
	repositoryEntity := new(repository.Repository)
	botWidget := new(widgets.BotWidget)
	repositoryEntity.SetDb(&c)
	handler.SetRepository(repositoryEntity)
	botWidget.SetRepository(repositoryEntity)
	handler.SetWidget(botWidget)
	handler.Handle(updates, bot)
	err := c.Close()
	helpers.CheckErr(err)
}
