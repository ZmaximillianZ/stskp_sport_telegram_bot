create table type_workout
(
    id          integer constraint table_name_pk primary key autoincrement,
    name        text,
    description text,
    parent_id   int references type_workout,
    type        int
);

create table type_workout_template
(
    id              integer constraint type_workout_template_pk primary key autoincrement,
    type_workout_id integer references type_workout on delete set null,
    format          INTEGER not null
);

create table user
(
    uid        INTEGER primary key autoincrement,
    first_name VARCHAR(64),
    last_name  VARCHAR(64),
    nick_name  VARCHAR(64),
    date_birth DATE,
    created    DATE,
    user_name  varchar(64)
);

create table workout
(
    id          INTEGER constraint workout_pk primary key autoincrement,
    user_id     integer references user on delete set null,
    created     date,
    description TEXT
);

create table type_workout_value
(
    id                       integer constraint type_workout_value_pk primary key autoincrement,
    workout_id               integer references workout on delete set null,
    type_workout_template_id integer references type_workout_template  on delete set null,
    value                    real not null
);

